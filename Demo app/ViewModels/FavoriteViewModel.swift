//
//  FavoriteViewModel.swift
//  Demo app
//
//  Created by Rain on 2023.06.24.
//

import UIKit

class FavoriteViewModel: BaseViewModel {
    private var favoriteData:Search!
    var bindFavoriteViewModel : (ConnectionStatus, String?) -> () = { status, reason in }
    var shouldLoad = false
    override init() {
        super.init()
        self.apiService = APIServices()
    }
    init(config:Config){
        super.init()
        self.apiService = APIServices()
        self.config = config
    }
    func getFavoriteData(page:Int){
        if page == Constants.FIRST_PAGE{
            self.favoriteData = nil
        }
        self.apiService.getFavorites(page: page) { response in
            let tempData = response
            self.shouldLoad = tempData.page ?? 0 < tempData.total_pages ?? 0 ? true:false
            if(self.favoriteData == nil){
                self.favoriteData = tempData
            }else{
                self.favoriteData.results? += tempData.results!
                self.favoriteData.page = tempData.page
            }
            self.bindFavoriteViewModel(.completed, nil)
        } failure: { error in
            self.shouldLoad = false
            self.bindFavoriteViewModel(.failed, error?.localizedDescription)
        }
    }
    func refreshData(){
        self.favoriteData = nil
        getFavoriteData(page: Constants.FIRST_PAGE)
    }
    func numberOfItems() -> Int{
        if self.favoriteData == nil { return 0 }
        return self.favoriteData.results?.count ?? 0
    }
    func setCellData(cell:TableViewCell, index:Int){
        if self.favoriteData.results?.count ?? 0 > index{
            let data = self.favoriteData.results?[index]
            cell.titleLabel.text = data?.title ?? ""
            cell.descLabel.text = data?.release_date ?? ""
            cell.ratingLabel.text = String(format:"%.1f/10", data?.vote_average ?? 0.0)
            cell.iconImageView.image = nil
            DispatchQueue.main.async {
                let urlString = self.getImageUrl(.thumbnail).appending((data?.poster_path ?? ""))
                cell.iconImageView.load(url:URL(string: urlString)!, placeholder: UIImage(named: "movie-poster-placeholder"), cache: URLCache()) {
                } failure: {
                    cell.iconImageView.image = UIImage(named: "movie-poster-placeholder")
                }
            }
        }
    }
    func selectMovie(index:Int){
        self.selectedResult = self.favoriteData.results?[index]
    }
    func checkFavorite(movieId:Int?) -> Bool{
        if favoriteData == nil { return false }
        let contain = (self.favoriteData.results?.contains(where: { result in
            return result.id == movieId
        }))!
        return contain
    }
    func loadNext(){
        if self.shouldLoad{
            self.shouldLoad = false
            self.getFavoriteData(page: self.favoriteData.page! + 1)
        }
    }
    func getSelectedMovie() -> Result{
        return self.selectedResult
    }
}
