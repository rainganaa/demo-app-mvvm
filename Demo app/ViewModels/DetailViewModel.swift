//
//  DetailViewModel.swift
//  Demo app
//
//  Created by Rain on 2023.06.24.
//

import UIKit
import Foundation

class DetailViewModel: BaseViewModel {
    var bindFavorite : (ConnectionStatus, String?) -> () = { status, text in}
    var bindDetailViewModel : (ConnectionStatus, String?) -> () = { status, text in}
    var movie:Movie!{
        didSet{
            self.bindDetailViewModel(.completed, nil)
        }
    }
    var isFavorite:Bool!
    override init() {
        super.init()
        self.apiService = APIServices()
    }
    init(result:Result?, config:Config){
        super.init()
        self.apiService = APIServices()
        self.selectedResult = result
        self.config = config
    }
    func getMovieDetail(){
        self.apiService.getMovieDetail(movieId: self.selectedResult.id!) { response in
            self.movie = response
        } failure: { error in
            self.bindDetailViewModel(.failed, error?.localizedDescription)
        }
    }
    func setBackground(imageView:UIImageView){
        let urlString = self.getImageUrl(.poster).appending((selectedResult.poster_path ?? ""))
        imageView.load(url:URL(string: urlString)!, placeholder: nil, cache: URLCache()) {
        } failure: {
        }
    }
    func setupContents(posterImageView:UIImageView, titleLabel:UILabel, dateLabel:UILabel,ratingLabel:UILabel, overViewLabel:UILabel){
        let urlString = self.getImageUrl(.poster).appending((self.movie.poster_path ?? ""))
        posterImageView.load(url:URL(string: urlString)!, placeholder: UIImage(named: "movie-poster-placeholder"), cache: URLCache()) {
            posterImageView.image = UIImage(named: "movie-poster-placeholder")
        } failure: {
        }
        titleLabel.text = self.movie.title
        let genres = self.movie.genres?.map {$0.name!}.joined(separator: ", ")
        dateLabel.text = "\(self.movie.release_date ?? "") · \(self.minuteToHourAndMinutes(minutes:self.movie.runtime)) · \(genres!)"
        ratingLabel.text = String(format:"%.1f/10", self.movie?.vote_average ?? 0.0)
        overViewLabel.text = self.movie.overview
    }
    func getMovieId()->Int{
        return self.selectedResult.id ?? 0
    }
    func checkItemFavorited() -> Bool{
        return self.isFavorite
    }
    func setFavorite(favoriteViewModel:FavoriteViewModel){
        self.isFavorite = favoriteViewModel.checkFavorite(movieId: self.selectedResult.id)
    }
    func sendFavorite(){
        self.apiService.addToFavorite(movieId: selectedResult.id ?? 0, favorite: !self.isFavorite) { response in
            if response.success ?? true{
                if response.status_code == Constants.FAVORITE_STATUS_CODE_ADDED{
                    self.bindFavorite(.completed, response.status_message)
                    self.isFavorite = true
                }else{
                    self.bindFavorite(.completed, response.status_message)
                    self.isFavorite = false
                }
            }else{
                self.bindFavorite(.failed, response.status_message)
            }
        } failure: { error in
            self.bindFavorite(.failed, error?.localizedDescription)
        }
    }
}
