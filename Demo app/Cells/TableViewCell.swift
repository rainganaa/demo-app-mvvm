//
//  TableViewCell.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if(highlighted){
            UIView.animate(withDuration: 0.1) {
                self.contentView.alpha = 0.3
            }
        }else{
            UIView.animate(withDuration: 0.1) {
                self.contentView.alpha = 1
            }
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.iconImageView.image = nil
    }
}
