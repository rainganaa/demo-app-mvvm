//
//  FavoriteViewController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit
class FavoriteViewController: BaseController {
    @IBOutlet weak var tableView: UITableView!
    var favoriteViewModel:FavoriteViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        callFavoriteViewModel()
    }
    func callFavoriteViewModel(){
        self.favoriteViewModel.bindFavoriteViewModel = { status, reason in
            DispatchQueue.main.async {
                self.tableView.allowsSelection = true
                if status == .failed {
                    self.showDropAlert(title: nil, message: reason, type: .unsuccessful)
                }else{
                    self.tableView.reloadData()
                }
            }
        }
    }
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailViewController = segue.destination as! DetailViewController
        detailViewController.detailViewModel = self.favoriteViewModel.createDetailViewModel()
        detailViewController.favoriteViewModel = self.favoriteViewModel
        detailViewController.didClosed = {
            DispatchQueue.main.async {
                self.tableView.allowsSelection = false
                self.favoriteViewModel.refreshData()
            }
        }
    }
}
extension FavoriteViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if favoriteViewModel == nil { return 0 }
        return self.favoriteViewModel.numberOfItems()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as? TableViewCell{
            self.favoriteViewModel.setCellData(cell: cell, index: indexPath.row)
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.favoriteViewModel.selectMovie(index: indexPath.row)
        performSegue(withIdentifier: "segueFavoriteToDetail", sender: self)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = self.tableView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = self.tableView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            self.favoriteViewModel.loadNext()
        }
    }
}
