//
//  DetailViewController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit

class DetailViewController: BaseController {
    var result:Result!
    var detailViewModel:DetailViewModel!
    var favoriteViewModel:FavoriteViewModel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var blurImageView: BlurImageView!
    @IBOutlet weak var ratingLabel: UILabel!
    var didClosed : (() -> ()) = {}
    override func viewDidDisappear(_ animated: Bool) {
        self.didClosed()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        callDetailViewModel()
    }
    func callDetailViewModel(){
        self.detailViewModel.setFavorite(favoriteViewModel: self.favoriteViewModel)
        self.detailViewModel.bindFavorite = { status, reason in
            DispatchQueue.main.async {
                self.animateButton(enable: true)
                self.updateButtonTitle()
                if status == .completed{
                    self.showDropAlert(title: nil, message: reason, type: .successful)
                }else{
                    self.showDropAlert(title: nil, message: reason, type: .unsuccessful)
                }
            }
        }
        self.detailViewModel.bindDetailViewModel = { status, reason in
            DispatchQueue.main.async {
                if status == .completed{
                    self.setupContents()
                    self.updateButtonTitle()
                }else{
                    self.showDropAlert(title: nil, message: reason, type: .unsuccessful)
                }
            }
        }
        self.detailViewModel.getMovieDetail()
        setupBlurImageView()
    }
    
    func setupBlurImageView(){
        detailViewModel.setBackground(imageView: blurImageView.imageView)
    }
    func setupContents(){
        detailViewModel.setupContents(posterImageView: posterImageView, titleLabel:titleLabel, dateLabel: dateLabel,ratingLabel:ratingLabel, overViewLabel: overviewLabel)
    }
    func updateButtonTitle(){
        favoriteButton.setTitle(self.detailViewModel.checkItemFavorited() ? Strings.buttonTitleUnFavorite: Strings.buttonTitleFavorite, for: .normal)
    }
    func animateButton(enable:Bool){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut) {
            self.favoriteButton.alpha = enable ? 1:0.8
        } completion: { completed in
            self.favoriteButton.isEnabled = enable
        }
    }
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        self.animateButton(enable: false)
        self.detailViewModel.sendFavorite()
    }
}
