//
//  BaseController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//
import UIKit
import Foundation
import SPIndicator
class BaseController: UIViewController{
    var api:APIServices!
    public var config:Config!
    @IBOutlet weak var placeHolderView: SearchPlaceHolderView!
    enum DropAlertType:String{
        case successful
        case unsuccessful
        case info
        case text
    }
    enum PlaceHolderType:String{
        case search
        case noResult
        case notFavorited
        case hide
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        api = APIServices()
    }
    public func showDropAlert(title:String?, message:String?, type:DropAlertType, completion: (() -> Void)? = nil){
        let indicatorView = SPIndicatorView(title: title ?? "", message: message)
        indicatorView.dismissByDrag = true
        indicatorView.subtitleLabel?.numberOfLines = 0
        DispatchQueue.main.async {
            switch type {
            case .successful:
                indicatorView.present(haptic: .success) {
                    (completion ?? {})()
                }
            case .unsuccessful:
                indicatorView.present(haptic: .error) {
                    (completion ?? {})()
                }
            case .info:
                indicatorView.present(haptic: .warning) {
                    (completion ?? {})()
                }
            case .text:
                indicatorView.present(haptic: .none) {
                    (completion ?? {})()
                }
            }
            
        }
    }
    func showPlaceHolder(type:PlaceHolderType){
        switch type {
            case .search:
                self.setPlaceHolder(text: nil, image: UIImage(named:"manage_search-manage_search_symbol"))
            case .noResult:
                self.setPlaceHolder(text: Strings.searchNoResult, image: UIImage(named:"manage_search"))
            case .notFavorited:
                self.setPlaceHolder(text: Strings.favoriteEmpty, image: UIImage(named:"book"))
            case .hide:
                self.hidePlaceHolder()
        }
    }
    func setPlaceHolder(text:String?, image:UIImage?){
        self.view.bringSubviewToFront(self.placeHolderView)
        self.placeHolderView.isHidden = false
        self.placeHolderView.imageView.image = image
        self.placeHolderView.label.text = text
    }
    func hidePlaceHolder(){
        self.placeHolderView.isHidden = true
    }
}
