//
//  ViewController.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import UIKit

class ViewController: BaseController{
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    private var searchViewModel:SearchViewModel!
    private var favoriteViewModel:FavoriteViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        callSearchViewModel()
    }
    func callSearchViewModel(){
        searchViewModel = SearchViewModel()
        searchViewModel.bindConfig = { status in
            if status == .completed{
                self.callFavoriteViewModel()
            }
        }
        searchViewModel.bindSearchViewModel = { status, reason in
            DispatchQueue.main.async {
                if status == .failed {
                    self.showDropAlert(title: nil, message: reason, type: .unsuccessful)
                }else{
                    self.tableView.reloadData()
                }
            }
        }
    }
    func callFavoriteViewModel(){
        self.favoriteViewModel = self.searchViewModel.createFavoriteViewModel()
        self.favoriteViewModel.getFavoriteData(page: Constants.FIRST_PAGE)
    }
    @IBAction func favoriteButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "segueToFavorite", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetail"{
            let detailViewController = segue.destination as! DetailViewController
            detailViewController.detailViewModel = self.searchViewModel.createDetailViewModel()
            detailViewController.favoriteViewModel = self.favoriteViewModel
            detailViewController.didClosed = {
                DispatchQueue.main.async {
                    self.favoriteViewModel.getFavoriteData(page: Constants.FIRST_PAGE)
                }
            }
        }else{
            let favoriteViewController = segue.destination as! FavoriteViewController
            favoriteViewController.favoriteViewModel = favoriteViewModel
        }
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchViewModel == nil { return 0 }
        self.showPlaceHolder(type: searchViewModel.showPlaceHolder() ? .noResult:.hide)
        return self.searchViewModel.numberOfItems()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as? TableViewCell{
//            DispatchQueue.main.async {
                self.searchViewModel.setCellData(cell: cell, index: indexPath.row)
//            }
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchViewModel.selectMovie(index: indexPath.row)
        searchBar.endEditing(true)
        performSegue(withIdentifier: "segueToDetail", sender: self)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = self.tableView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = self.tableView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            self.searchViewModel.loadNext()
        }
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        return true
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchViewModel.searchBarEdited(text: searchText)
    }
}
