//
//  Constants.swift
//  Demo app
//
//  Created by Rain on 2023.06.20.
//

import Foundation
class Constants{
    static let HOST = URL(string:"https://api.themoviedb.org")!
    static let API_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIxNWY5YTdiMDk2ZmYyOTg1MzQ2Nzc1ZDhiOTgzZGYyZiIsInN1YiI6IjY0OTExN2Y3MjYzNDYyMDBlYjc3YTQ1NyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.Ifc0Wc54OQxxPJI5zUSVziNg9-LM2gXx4JDGydl0Uoo"
    static let API_KEY = "15f9a7b096ff2985346775d8b983df2f"
    static let FAVORITE_STATUS_CODE_ADDED = 1
    static let FAVORITE_STATUS_CODE_REMOVED = 13
    static let FIRST_PAGE = 1
    
}
